FROM mhart/alpine-node:0.10.48

RUN apk --update add git openssh

RUN git clone https://bitbucket.org/datenfreunde/lobbyplag-data.git /lobbyplag-data
RUN mkdir /lobbyplag-sites -p

COPY ./frontends /lobbyplag-sites/frontends
COPY ./generators /lobbyplag-sites/generators

COPY docker-config.js /lobbyplag-sites/config.js
COPY docker-package.json /lobbyplag-sites/package.json

RUN cd /lobbyplag-sites && npm i
RUN mkdir /lobbyplag-sites/frontends/browse/assets/data -p && mkdir /lobbyplag-sites/frontends/browse/assets/tmpl -p


# create datasources for frontend
RUN cd /lobbyplag-sites/ && node generators/browse/compile.js
RUN cd /lobbyplag-sites/ && node generators/docs/compile.js
RUN cd /lobbyplag-sites/ && node generators/research/compile.js

RUN cd lobbyplag-sites/frontends && \
for DIR in browse classify crowd docs index influence map research governments; do\
 cp $DIR/config.dist.js $DIR/config.js; \
done \

