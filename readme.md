# LobbyPlag Sites

This repository contains the websites of the [LobbyPlag](http://www.lobbyplag.eu/) project and scripts to update them.

## Generators

These directories hold the scripts to transform data from the 
[Data Repository](https://github.com/lobbyplag/lobbyplag-data) 
to the data formats the sites use.

There is also a generator for `plag`, which will eventually replace the main site.


# setup

- get node v0.10.26

- create all config.js (from config.dist.js) in lobbyplag-sites/frontends/*
- set paths in lobbyplag-sites/config.js


run

    cd lobbyplag-sites
    mkdir frontends/browse/assets/data -p
    mkdir frontends/browse/assets/tmpl -p

    npm i
    
    node generators/browse/compile.js
    node generators/docs/compile.js
    node generators/research/compile.js
    

# starting a service

    # cp frontends/<service>/config.dist.js frontends/<service>/config.js 
    node frontends/<service>/<service>.js
    

## running in docker

see https://bitbucket.org/datenfreunde/lobbyplag-docker.
this repo has a docker-compose file which bundles the separate services via nginx

## making a new release for dockerhub

    docker build . -t datenfreundegmbh/lobbyplag-sites
    
    docker tag lobbyplag-sites datenfreundegmbh/lobbyplag-sites
    docker push datenfreundegmbh/lobbyplag-sites
    
(todo: this does not contain any versioning
)