module.exports = {
	"prefix": "/browse",
	"baseurl": "http://lobbyplag.eu/browse",
	"hostname": "0.0.0.0",
	"port": 9903,
	"locales": ["bg","cs","da","de","el","en","es","et","fi","fr","ga","hu","it","lt","lv","mt","nl","pl","pt","ro","sk","sl","sv"],
	"locale_default": "en"
}