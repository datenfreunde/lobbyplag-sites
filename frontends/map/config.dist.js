module.exports = {
	"prefix": "/map",
	"hostname": "0.0.0.0",
	"port": 9908,
	"datadir": "../../../lobbyplag-data/data",
	"baseurl": "http://lobbyplag.eu/map",
	"debug": false
}