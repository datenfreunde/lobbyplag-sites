 $(window).load(function () {

var pop_content = "", data, pop_pro = "", pop_contra = ""; pop_gruen= ""; pop_rot="";
var nation_names ='{AT:"AUSTRIA",BE:"BELGIUM",BG:"BULGARIA",CH:"SWITZERLAND",CY:"CYPRUS",CZ:"CZECH REPUBLIC",DE:"GERMANY",DK:"DENMARK",EE:"ESTONIA",ES:"SPAIN",FI:"FINLAND",FR:"FRANCE",UK:"UNITED KINGDOM",GR:"GREECE",HR:"CROATIA",HU:"HUNGARY",IE:"IRELAND",IS:"ICELAND",IT:"ITALY",LI:"LIECHTENSTEIN",LT:"LITHUANIA",LU:"LUXEMBOURG",LV:"LATVIA",MT:"MALTA",NL:"NETHERLANDS",NO:"NORWAY",PL:"POLAND",PO:"PORTUGAL",PT:"ROMANIA",RO:"ROMANIA",SE:"SWEDEN",SI:"SLOVENIA",SK:"SLOVAKIA",COM:"COMMISSION"}';
var nations = Function("a="+nation_names+"; return a")();

var ci=0;

$(".maxremark").each(function(i,element) {
	var $e=$(element);	
	data=$e.data();	 	
	
	/*POPUP CONTENT*/

	/* die "roten" und "grünen" */
	var rot,gruen,pop_content,pro,contra,pop_rot,pop_gruen;
	pop_rot="";
	pop_gruen="";
	pop_content="";

	if (data.wert=="w") { /* weniger Datenschutz */ 
		rot=data.pro;
		gruen=data.contra;
	} else {
		if (data.wert=="m") { /* mehr Datenschutz */ 
			rot=data.contra;
			gruen=data.pro;
		}
	}

	/*icons START*/
	if (gruen !== undefined ){	
		pop_gruen=Mustache.to_html('{{#c}}<a title="" href="/{{path}}#page={{page}}"><img class="cp" src="assets/img/icons/{{country}}_g_plakette.png" width="30" title="View {{name}} in document No. {{docid}} at page {{ page }}"></a> {{/c}} ',{ c: gruen})
		/* pro = gruen;
		$.each(pro, function(n,m) { 		
			//pop_gruen = pop_gruen + "<a href='"+data.modal+"' class='nation pro'><img src='assets/img/icons/"+n+"_g_plakette.png' width='30' title='"+nations[n]+": pro privacy'></a> ";	
			pop_gruen = pop_gruen + "<a class='modallink' href='#regmodal' id='"+data.id+"' role='button' data-toggle='modal'><img src='assets/img/icons/"+n+"_g_plakette.png' width='30' title='"+nations[n]+": pro privacy'></a> ";	
			});		*/
	}
	
	if (rot !== undefined ){	
		pop_rot=Mustache.to_html('{{#c}}<a title="" href="/{{path}}#page={{page}}"><img class="cp" title="View {{name}} in document No. {{docid}} at page {{ page }}"src="assets/img/icons/{{country}}_r_plakette.png" width="30" title="{{name}}"></a>{{/c}} ',{ c: rot})
/*		contra = rot;
		$.each(contra, function(n,m) { 
			//pop_rot = pop_rot + "<a href='"+data.modal+"' class='nation contra'><img src='assets/img/icons/"+n+"_r_plakette.png'  width='30' title='"+nations[n]+": against privacy'></a> ";			
			pop_rot = pop_rot + " <a class='modallink' href='#regmodal' id='"+data.id+"' role='button' data-toggle='modal'><img src='assets/img/icons/"+n+"_r_plakette.png'  width='30' title='"+nations[n]+": against privacy'></a> ";			
			});	*/
	}	
		
	if (data.wert === "w" && pop_rot!==""){
		pop_content = pop_content + pop_rot+ "<hr class='line'>";	
	} 
	
	if (data.wert === "m" && pop_gruen!==""){
		pop_content = pop_content +  pop_gruen+ "<hr class='line'>";		
	} 
	
	if (data.wert === "n"){
		/* 
		if (pop_rot!==""){
			pop_content = pop_content + pop_rot; 
		}	
		if (pop_gruen!==""){
			if (pop_gruen!==""){
				pop_content = pop_content  + "<br>"; 
				}
			pop_content = pop_content + pop_rot; 
		}
		if (pop_content!==""){
			pop_content = pop_content + "<hr class='line'>"; 
		} */
	} 
	
	/*icons END*/
	
	if (data.text !== undefined && data.text.length>0){	
		pop_content =  pop_content +data.text;
	}
	

	if (data.topic!= undefined) {	
		topic = data.topic
		pop_content =  pop_content +"<br/>See also: <a href='"+ topic.url +"' class='topic' title='see "+topic.title+"'></a><a href='"+topic.url+"'>"+topic.title+"</a>" ;
	}

	if (data.wert != "n") {
		
		if (data.document != undefined){	
			var doc = data.document
			pop_content =  pop_content +"<div><span>Source: <a class='doc_link' href='"+ doc.url +"' title='"+doc.file+" ("+doc.date+")'>Document "+doc.docn+"</a></span><div class='remarkid'><a title='link to this remark' href='/governments/gdpr?remark="+data.id+"'>&#10138;</a>&nbsp;<a class='popup_bug' href='mailto:error@lobbyplag.eu?Subject=Report%20Remark%20#"+data.id+"' target='_blank'>Report remark</a></div>" ;
		}
	/*	
		if (data.worse !== undefined && data.worse.length>0){	
			pop_content =  pop_content +"<br><a href='"+ data.modal +"' class='topic' title='worse than old regulation'><span class='worse'>WORSE</span></a>" ;
		}
	*/
	}
	
	if (data.wert === "w" && pop_gruen!==""){
		pop_content = pop_content +  "<br clear='all'/><hr class='line'>" +pop_gruen;		
	} 	
	
	if (data.wert === "m" && pop_rot!==""){
		pop_content = pop_content  + "<hr class='line'>"+ pop_rot;	
	} 
	
	
	/*ADD CONTENT AND RESET CONTENT*/


	where=ci%2 ? "left" : "right";

	/*TEXT COLORS AND PLACEMENT*/
	if (data.wert === "w"){
		$e.popover({
			placement : where, 
			html: 'true',
			trigger: 'manual',
			content: pop_content
		}).popover('show');		
		ci+=1;
	}
	if (data.wert === "n"){
		$e.popover({
	        placement : 'top', 
			html: 'true',
			trigger: 'hover',
			content: pop_content,
			delay: { show: 0, hide: 2500 }
		});	
	}
	if (data.wert === "m"){
		$e.popover({
			placement : where, 
			html: 'true',
			trigger: 'manual',
			content: pop_content
		}).popover('show');		
		ci+=1;
	}

});

	/*POPUP TO TOP*/		
	$(".popover").mouseover(function() {
				$(this).css('z-index',9999).addClass("beige");
		}).mouseout(function() {
				$(this).css('z-index',100).removeClass("beige");
		});	
		
	$(".maxremark").mouseover(function() {
		$(this).next( ".popover" ).css('z-index',9999).addClass("beige");
	}).mouseout(function() {
		$(this).next( ".popover" ).css('z-index',100).removeClass("beige");
	});
			
		
	/*MOVE POPUPS TO LEFT/RIGHT*/	
	var position = $(".walloftext").position();
	$(".popover.left").css ('left', position.left-260);	
	$(".popover.right").css ('left', position.left+478);	
	$(".popover").css ('max-width', 276);	
	$(".popover").css ('min-width', 276);	
	$(".cp").on("mouseover",function() {$(this).tooltip('show') });	 
	/* nimmt hoffentlich das Überlastungsproblem weg */ 
	
	/*MODAL*/	
		$( ".modallink" ).click(function() {
		var myid = $(this).attr( "id" );
		myid = myid.replace(".","-");
		
		var pro = $(".maxremark-"+myid).data("pro");
		var contra = $(".maxremark-"+myid).data("contra");
		
		var content="";		
		content = "<div class='row modal_box'><div class='span3'><h4>AGAINST PRIVACY</h4><ul>";		
		if (contra != undefined && contra.length>0){
			var con = Function("a="+contra+"; return a")();		
			$.each(con, function(o,p) { 
			//content = content + "<li><a href='"+p+"' class='nation contra'><img src='assets/img/icons/"+o+"_r_plakette.png'  width='30'  title='"+nations[o]+": against privacy'> "+nations[o]+": "+p.split("/")[2]+"</a></li>" ;
			
			
			content = content + " <li><a href='"+p+"' class='nation contra'> <img src='assets/img/icons/"+o+"_r_plakette.png'  width='30'  title='"+nations[o]+": against privacy'></a> "+
						" <a href='"+p+"' class='nation topic_modal'>" +nations[o]+":"+p.split("/")[2]+"</a></li>";
			});	
		} else {content = content + "<li>no documents<l/i>"};
		
		content = content + "</ul></div><div class='span3'><h4>PRO PRIVACY</h4><ul>";
	
		if (pro != undefined && pro.length>0){
				var p = Function("a="+pro+"; return a")();
				$.each(p, function(s,d) { 
			//	content = content + "<li><a href='"+d+"' class='nation pro'><img src='assets/img/icons/"+s+"_g_plakette.png'  width='30'  title='"+nations[s]+": against privacy'> "+nations[s]+": "+d.split("/")[2]+"</a></li>" ;
				content = content + " <li><a href='"+d+"' class='nation pro'> <img src='assets/img/icons/"+s+"_g_plakette.png'  width='30'  title='"+nations[s]+": pro privacy'></a> "+
						" <a href='"+d+"' class='nation topic_modal'> "+nations[s]+":"+d.split("/")[2]+"</a></li>";
			});	
		} else {content = content + "<li>no documents</li>"};
			content = content + "</ul></div></div>"
		
		$( "#regmodal_content" ).html(content);
	});

	var where=document.location.href.match(/remark=([0-9\.]+)/);
	if (where) {
		tries=0;
		var here=".maxremark-"+where[1].replace(".","-");
		var mi=setInterval(function() {	
			if ($(here)) {
				$(here).addClass("linked")[0].scrollIntoView();
				var $d=$(document);
				$d.scrollTop($d.scrollTop()-300);
				clearInterval(mi);
			} else {
				tries=tries+1;
				if (tries>100) {
					clearInterval(mi);
				}
			}
			console.log("interval");
		},100);
	}

});




