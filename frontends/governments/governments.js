var path = require('path')
	, fs = require('fs')
	, url = require('url')
	, querystring = require("querystring")
	, underscore = require('underscore')
	, mustache = require('mustache')
	, express = require('express')
	, serveStatic=require('serve-static')
	, serveFavicon=require('serve-favicon')
	, compression = require('compression')
	, bodyParser = require('body-parser')
	, logger = require('morgan');

var config = require(path.resolve(__dirname, './config.js'));

Array.prototype.findByUID = function (uid) {
	for (var i = 0; i < this.length; i++) {
		if (this[i].uid === uid) {
			return this[i];
		}
	}
	return null;
};

Array.prototype.findByID = function (id) {
	for (var i = 0; i < this.length; i++) {
		if (this[i].id === id) {
			return this[i];
		}
	}
	return null;
};

String.prototype.expand = function () {
	return this.toString().replace(/([hrcaspit])/g,function (l) {
		switch (l) {
			case "h":
				return "|Title ";
				break;
			case "r":
				return "|Recital ";
				break;
			case "c":
				return "|Chapter ";
				break;
			case "s":
				return "|Section ";
				break;
			case "a":
				return "|Article ";
				break;
			case "p":
				return "|Paragraph ";
				break;
			case "i":
				return "|Point ";
				break;
			case "t":
				return "|Text ";
				break;
		}
	}).replace(/^\|/, '').split(/\|/g).join(" – ");
};




function ArticleList(value) {
	var me = value || [];

	me.initArticles = function (classified, groups) {
		var _articles = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'data', 'articles.json')));
		_articles.forEach(function (article) {
			article.directive = 'Article ' + article.nr;
			article.classified = article.classified || ClassifyList();
			classified.forEach(function (c) {
				var test = c.amend.directive.split(' – ')[0].split('+')[0];
				if (test === article.directive) {
					article.classified.push(c);
				}
			});
			article.overview_meps = {};

			article.classified.forEach(function (c) {
				article.overview_meps[c.vote] = article.overview_meps[c.vote] || [];
				if (c.meps) {
					c.meps.forEach(function (mep) {
						var obj = article.overview_meps[c.vote].findByID(mep.id);
						if (!obj) {
							obj = {id: mep.id, mep: mep, count: 0 };
							article.overview_meps[c.vote].push(obj);
						}
						obj.amend_nrs = obj.amend_nrs || [];
						obj.amend_nrs.push(c.amend.number);
						obj.count += 1;
					});
				} else {
					console.log('Error 1');
				}
				article.overview_meps[c.vote].sort(function (a, b) {
					if (a.count > b.count)
						return -1;
					if (a.count < b.count)
						return 1;
					return 0;
				});
			});
			article.overview = article.classified.getClassifiedOverview();
			article.group_overview = GroupOverList();
			groups.forEach(function (group) {
				article.group_overview.addGroupOverview(
					group, article.classified.filterClassifiedByGroup(group.id).getClassifiedOverview
				)
			});
			if (article.classified.length > 0)
				me.push(article);
		});
		_articles = _articles.filter(function (article) {
			return (article.classified) && (article.classified.length > 0);
		});
		_articles.sort(function (a, b) {
			if (a.nr < b.nr)
				return -1;
			if (b.nr < a.nr)
				return 1;
			return 0;
		});
		for (var i = 0; i < _articles.length; i++) {
			if (i > 0)
				_articles[i].prev = {nr: _articles[i - 1].nr, title: _articles[i - 1].directive};
			if (i < _articles.length - 1)
				_articles[i].next = {nr: _articles[i + 1].nr, title: _articles[i + 1].directive};
		}
	};

	me.findArticleByNr = function (searchnr) {
		for (var i = 0; i < me.length; i++) {
			if (me[i].nr === searchnr) {
				return me[i];
			}
		}
		return null;
	};

	return me;
}




/* configure Express */

var app = express();
var _data = JSON.parse(fs.readFileSync(path.resolve(__dirname, config.datadir,"lobbyplag-governments-metadata.json" )));



/* app.configure(function () { */
	app.use(serveFavicon(__dirname + '/assets/img/favicon.ico'));
	app.use(compression());
	app.use(bodyParser.urlencoded({extended: true}));
	app.use(bodyParser.json());
	app.use("/assets", serveStatic(path.resolve(__dirname, 'assets')));
	app.use("/governments/assets", serveStatic(path.resolve(__dirname, 'assets')));
	if (config.debug) {
		app.use(logger('dev'));
	} else {
		app.use(logger('combined'));
	}
/* }); */ 

/* sending & cache */

var send = function (req, res, data) {
	res.setHeader('Content-Type', 'text/html; charset=utf-8');
	if (!config.debug) {
		var oneHour = 3600;
		res.setHeader('Cache-Control', 'public, max-age=' + (oneHour));
	}
	res.send(data);
};

var cache = {};

function cache_send(req, res, key) {
	var _data = cache[key];
	if (_data) {
		_data.called += 1;
		send(req, res, _data.content);
		return true;
	}
	return false;
}

function cache_store(key, content) {
	cache[key] = {called: 1, content: content};
}

/* templates */

var tmpl = {
	index: fs.readFileSync(path.resolve(__dirname, "tmpl/index.mustache")).toString(),
	header: fs.readFileSync(path.resolve(__dirname,  "tmpl/header.mustache")).toString(),
	subnav: fs.readFileSync(path.resolve(__dirname,  "tmpl/subnav.mustache")).toString(),
	footer: fs.readFileSync(path.resolve(__dirname,  "tmpl/footer.mustache")).toString(),

	imprint: fs.readFileSync(path.resolve(__dirname,  "tmpl/imprint.mustache")).toString(),
	dataprotection: fs.readFileSync(path.resolve(__dirname,  "tmpl/dataprotection.mustache")).toString(),
	
	regulation: fs.readFileSync(path.resolve(__dirname, "tmpl/regulation.mustache")).toString(),
	regulationtext: fs.readFileSync(path.resolve(__dirname, "tmpl/regulationtext.mustache")).toString(),

	"document": fs.readFileSync(path.resolve(__dirname, "tmpl/document.mustache")).toString(),
	modal: fs.readFileSync(path.resolve(__dirname, "tmpl/modal.mustache")).toString(),
	overview: fs.readFileSync(path.resolve(__dirname, "tmpl/overview.mustache")).toString(),
	countries: fs.readFileSync(path.resolve(__dirname, "tmpl/countries.mustache")).toString(),
	articles: fs.readFileSync(path.resolve(__dirname, "tmpl/articles.mustache")).toString(),
	article: fs.readFileSync(path.resolve(__dirname, "tmpl/article.mustache")).toString(),
	meps: fs.readFileSync(path.resolve(__dirname, "tmpl/meps.mustache")).toString(),
	group: fs.readFileSync(path.resolve(__dirname, "tmpl/group.mustache")).toString(),
	method: fs.readFileSync(path.resolve(__dirname, "tmpl/method.mustache")).toString(),
	about: fs.readFileSync(path.resolve(__dirname, "tmpl/about.mustache")).toString(),
	topics: fs.readFileSync(path.resolve(__dirname, "tmpl/topics.mustache")).toString(),
	documents: fs.readFileSync(path.resolve(__dirname, "tmpl/documents.mustache")).toString(),
	list: fs.readFileSync(path.resolve(__dirname, "tmpl/list.mustache")).toString(),
	mailtest: fs.readFileSync(path.resolve(__dirname, "tmpl/mailtest.mustache")).toString(),
	discuss: fs.readFileSync(path.resolve(__dirname, "tmpl/discuss.mustache")).toString(),
	amends: fs.readFileSync(path.resolve(__dirname, "tmpl/amends.mustache")).toString(),

	partial_amend_overview_text: fs.readFileSync(path.resolve(__dirname, "tmpl/partial_amend_overview_text.mustache")).toString(),
	partial_article_mep: fs.readFileSync(path.resolve(__dirname, "tmpl/partial_article_mep.mustache")).toString(),
	partial_map: fs.readFileSync(path.resolve(__dirname, "tmpl/partial_map.mustache")).toString(),
	partial_pie: fs.readFileSync(path.resolve(__dirname, "tmpl/partial_pie.mustache")).toString(),
	partial_group_bar: fs.readFileSync(path.resolve(__dirname, "tmpl/partial_group_bar.mustache")).toString(),
	partial_group_bar_local: fs.readFileSync(path.resolve(__dirname, "tmpl/partial_group_bar_local.mustache")).toString(),
	partial_mep_line: fs.readFileSync(path.resolve(__dirname, "tmpl/partial_mep_line.mustache")).toString()
};

var fillTemplate = function (template, _data) {
	/* _data.active_map = true; */ 
	_data.disable_email = true;
	return mustache.render(tmpl.index, _data, {
		main: template,
		partial_mep_line: tmpl.partial_mep_line,
		partial_group_bar: tmpl.partial_group_bar,
		partial_map: tmpl.partial_map,
		partial_article_mep: tmpl.partial_article_mep,
		partial_amend_overview_text: tmpl.partial_amend_overview_text,
		partial_group_bar_local: tmpl.partial_group_bar_local,
		partial_pie: tmpl.partial_pie,
		"header": tmpl.header,
		"footer": tmpl.footer,
		"subnav": tmpl.subnav,
		"regulationtext" : tmpl.regulationtext
	})
};

var sendTemplate = function (req, res, template, data) {
	send(req, res, fillTemplate(template, data));
};



var sendBareTemplate = function (req, res, template, data) {
	send(req, res, mustache.render(template,data));
};

/* index */

function sendIndex(req, res) {
	if (cache_send(req, res, 'index'))
		return;
	var _page = fillTemplate(tmpl.overview, {active_overview: true, countries: _data.countries, eumap: _data.countrymap, ranking : _data.countries_ranking, total: _data.countries_total  });
	send(req, res, _page);
	cache_store('index', _page);
}

app.get(config.prefix, function (req, res) {
	sendIndex(req, res);
});

app.get(config.prefix + '/overview', function (req, res) {
	sendIndex(req, res);
});


app.get(config.prefix + '/list', function (req, res) {
	sendTemplate(req,res, tmpl.countries, { countries : underscore.sortBy(_data.countries,"name"), title : "Governments", active_governments : true });
});

/* text sites */

app.get(config.prefix + '/method', function (req, res) {
	sendTemplate(req, res, tmpl.method, {active_method: true, title: 'Method'})
});

app.get(config.prefix + '/about', function (req, res) {
	sendTemplate(req, res, tmpl.about, {active_about: true, title: 'About'})
});

app.get(config.prefix + '/gdpr', function (req, res) {
	sendTemplate(req, res, tmpl.regulation, {active_gdpr: true, title: 'Proposal'})
});

app.get(config.prefix + '/topics', function (req, res) {
	sendTemplate(req, res, tmpl.topics, {active_topics: true, title: 'Topics'})
});

app.get(config.prefix + '/documents', function (req, res) {
	sendTemplate(req, res, tmpl.documents, {active_documents: true, title: 'Documents', documents: _data.filelist_sorted})
});

app.get(config.prefix + '/list', function (req, res) {
	sendTemplate(req, res, tmpl.list, {active_governmets: true, title: 'Governments'})
});

app.get(config.prefix + '/imprint', function (req, res) {
	sendTemplate(req, res, tmpl.imprint, {})
});

app.get(config.prefix + '/dataprotection', function (req, res) {
	sendTemplate(req, res, tmpl.dataprotection, {})
});


app.get(config.prefix + '/document/:pid', function (req, res) {
	var pid=req.params.pid
	var docdata=_data.filelist_urlname[pid]
	docdata.countries=_data.votes_urlname[pid]
	docdata.mentions=_data.mentions[pid]

	if (pid && docdata) {
		sendTemplate(req,res,tmpl.document, { document : docdata, title: docdata.subject + " (" + docdata.docid + ")" });
	} else {
		sendTemplate(req,res,tmpl.document, { document : { title: "No doc with id "+pid } , title: "Not Found: id "+id});
	}
});

app.get(config.prefix + '/modal/:pid', function (req, res) {
	var pid=req.params.pid
	var datapoint=_data.datapoints[pid]

	if (pid && datapoint) {
		sendBareTemplate(req,res,tmpl.modal, { datapoint : datapoint, title: "Remark "+datapoint });
	} else {
		sendBareTemplate(req,res,tmpl.modal, { datapoint : { title: "No remark with id "+pid } , title: "Not Found: id "+pid});
	}
});
/* debug sites */

app.get(config.prefix + '/mailtest', function (req, res) {
	var params = [];
	lpdata.countries.forEach(function (county) {
		params.push(utils.compileMailParameter(county));
	});
	lpdata.groups.forEach(function (group) {
		params.push(utils.compileMailParameter(null, [group]));
	});
	params.push(utils.compileMailParameter(null, lpdata.groups));
	sendTemplate(req, res, tmpl.mailtest, {params: params});
});


/* Server */

app.listen(config.port, config.hostname);
console.log('Listen ' + config.hostname + ':' + config.port);
